Name:           perl-XML-Simple
Version:        2.25
Release:        6
Summary:        Easy API to maintain XML in Perl
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/XML-Simple
Source0:        https://cpan.metacpan.org/authors/id/G/GR/GRANTM/XML-Simple-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.76 perl(strict)
BuildRequires:  perl(warnings) perl(Carp) perl(Exporter) perl(File::Basename) perl(IO::Handle) perl(Scalar::Util)
BuildRequires:  perl(Storable) perl(vars) perl(warnings::register) perl(XML::NamespaceSupport) >= 1.04
BuildRequires:  perl(XML::Parser) perl(XML::SAX) >= 0.15 perl(base) perl(File::Spec) perl(File::Temp)
BuildRequires:  perl(FileHandle) perl(IO::File) perl(Test::More) perl(XML::SAX::Base) perl(Tie::IxHash)
Requires:       perl(File::Basename) perl(File::Spec)
Requires:       perl(IO::Handle) perl(Storable) perl(XML::NamespaceSupport) >= 1.04 perl(XML::Parser) perl(XML::SAX) >= 0.15

%description
The XML::Simple module provides a simple API layer on top of an underlying XML parsing module (either XML::Parser or one of the SAX2 parser modules).

%package        help
Summary:        Help documents for perl-XML-Simple

%description    help
The perl-XML-Simple-help package conatins manual pages for perl-XML-Simple.

%prep
%autosetup -n XML-Simple-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
chmod -R u+w %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc README
%{perl_vendorlib}/*

%files help
%doc Changes
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2.25-6
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jul 19 2024 yaoxin <yao_xin001@hoperun.com> - 2.25-5
- License compliance rectification

* Fri Dec 06 2019 liujing<liujing144@huawei.com> - 2.25-4
- Package init
